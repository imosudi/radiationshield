from flask import Flask, render_template, url_for 
from flask_bootstrap import Bootstrap


app = Flask(__name__)
Bootstrap(app)



@app.route('/')
def hello_world():
    #return 'Hello, World!'
    return render_template('index.html')

@app.route('/next')
def next():
	return render_template('nextpage.html')

@app.route('/register')
def registration():
	return render_template('form.html')

if __name__ == "__main__":
    app.run(debug=True)